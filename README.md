# LLVM Variables Track Pass

An LLVM pass to track variables.

Build:

    $ cd llvm-vartrack-pass
    $ mkdir build
    $ cd build
    $ cmake ..
    $ make
    $ cd ..

Run:

    $ clang -Xclang -load -Xclang build/vartrack/libVarTrackPass.* something.c
