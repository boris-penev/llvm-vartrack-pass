#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"
using namespace llvm;

namespace {
  struct VariablesTrack : public FunctionPass {
    static char ID;
    VariablesTrack() : FunctionPass(ID) {}

    bool runOnFunction(Function &F) {
      errs() << "I saw a function called " << F.getName() << "!\n";
      errs() << F << '\n';
      //for (inst_iterator ii = inst_begin(F), E = inst_end(F); ii != E; ++ii) {
        // errs() << *ii << '\n';
      //}
      Function::const_iterator const itfe = F.end();
      for (Function::const_iterator itf = F.begin(); itf != itfe; itf++) {
        // BasicBlock *bb = &*itf;
        for (BasicBlock::const_iterator in = itf->begin(), ine = itf->end(); in != ine; ++in) {
          errs() << "I saw a basic block!\n";
          in->dump();
          // errs() << '\n';
        }
      }
      return false;
    }
  };
}

char VariablesTrack::ID = 0;

static RegisterPass<VariablesTrack> variabletrack("vartrack",
  "Track values of variables");
